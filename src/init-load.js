const initialPageLoad = () => {
    const content = document.querySelector('#content'); // DIV to append to.
    
    const logo = document.createElement('img');
    logo.setAttribute('src', './logo.png');
    logo.setAttribute('alt', 'RESTAURANT');
    logo.setAttribute('id', 'logo');

    /*const title = document.createElement('h1');
    title.innerHTML = 'Restaurant Page';*/

    const tagLine = document.createElement('h3');
    tagLine.innerHTML = 'THE BEST RESTAURANT YOU WILL EVER EAT AT';
    tagLine.setAttribute('id','tag-line');
    
    content.append(logo, tagLine);
};

export {
    initialPageLoad
}