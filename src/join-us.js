const joinUs = () => {
    const tabContent = document.querySelector('.tab .tab-content');
    tabContent.innerHTML = '';

    const joinUsHeading = document.createElement('h3');
    joinUsHeading.innerHTML = 'HOW TO JOIN US';

    tabContent.append(
        joinUsHeading
    );
}

export {
    joinUs
}