/* Contact us tab module */
const contactUs = () => {
    const contactUsTab = document.querySelector('.tab .tab-content');
    contactUsTab.innerHTML = '';

    /* Create the address elements */
    const addressHeading = document.createElement('h3');
    const address = document.createElement('p');

    addressHeading.innerHTML = "HOW TO FIND US";
    address.innerHTML = "2a Restaurant Way, Dinner Town, DI1 N3R";

    /* Create contact form */
    const contactUsForm = document.createElement('form');

    const nameFieldLabel = document.createElement('label');
    nameFieldLabel.innerHTML = 'Enter your name';
    const nameField = document.createElement('input');

    const enquiryFieldLabel = document.createElement('label');
    enquiryFieldLabel.innerHTML = 'What is your question?';
    const enquiryField = document.createElement('textarea');

    const submit = document.createElement('button');
    submit.setAttribute('type', 'submit');
    submit.innerHTML = 'Send enquiry';

    contactUsForm.append(nameFieldLabel, nameField, enquiryFieldLabel, enquiryField, submit);

    contactUsTab.append(addressHeading, address, contactUsForm);

};

export {
    contactUs
}