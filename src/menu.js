const menu = () => {
    const tab = document.querySelector('.tab .tab-content');
    tab.innerHTML = '';
    
    const menuHeading = document.createElement('h3');
    menuHeading.innerHTML = 'FEAST YOUR EYES ON OUR DELICIOUS FOOD';

    const burger = document.createElement('p');
    burger.innerHTML = 'BURGER AND CHIPS £3.00';

    const salad = document.createElement('p');
    salad.innerHTML = 'SALAD £8.00';

    tab.append(
        menuHeading,
        burger,
        salad
    );

};

export {
    menu
}