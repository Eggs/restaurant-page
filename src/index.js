import { initialPageLoad } from './init-load';
import { contactUs } from './contact-us';
import { menu } from './menu';
import { joinUs } from './join-us';

console.log('[+] File loaded');

initialPageLoad();

/* Tab switch logic. */
const switchTabs = () => {
    const contactUsRadio = document.querySelector('#contact-us-radio');
    const menuRadio = document.querySelector('#menu-radio');
    const joinUsRadio = document.querySelector('#join-us-radio');

    contactUsRadio.addEventListener('click', (e) => {
        contactUs();
    });

    menuRadio.addEventListener('click', (e) => {
        menu();
    });

    joinUsRadio.addEventListener('click', (e) => {
        joinUs();
    });

};

switchTabs();
contactUs(); // Set inital tab.